#include <arm_cmse.h>
#include "M2351.h"
#include "cmsis_os2.h"
#include "partition_M2351.h"

#if defined (__ARM_FEATURE_CMSE) && (__ARM_FEATURE_CMSE == 3U)

#define NU_MFP_POS(pin)                             ((pin % 8) * 4)
#define NU_MFP_MSK(pin)                             (0xful << NU_MFP_POS(pin))

__NONSECURE_ENTRY
void pin_function_s(int32_t port_index, int32_t pin_index, int32_t data)
{  
    /* Guard access to secure GPIO from non-secure domain */
    if (cmse_nonsecure_caller() && 
        (! (SCU_INIT_IONSSET_VAL & (1 << (port_index + 0))))) {
        printf("Non-secure domain tries to control secure or undefined GPIO.");
    }

    __IO uint32_t *GPx_MFPx = ((__IO uint32_t *) &SYS->GPA_MFPL) + port_index * 2 + (pin_index / 8);
    uint32_t MFP_Msk = NU_MFP_MSK(pin_index);
    
    // E.g.: SYS->GPA_MFPL  = (SYS->GPA_MFPL & (~SYS_GPA_MFPL_PA0MFP_Msk) ) | SYS_GPA_MFPL_PA0MFP_SC0_CD  ;
    *GPx_MFPx  = (*GPx_MFPx & (~MFP_Msk)) | data;
}
#endif
#ifndef HAL_SECURE_H
#define HAL_SECURE_H

#include "M2351.h"

#ifdef __cplusplus
extern "C" {
#endif

/* pin_function (secure version)
 *
 * Guard access to secure GPIO from non-secure domain.
 * 
 * Its synopsis is the same as normal version except change of return/argument type for
 * binary-compatible across compilers.
 */
__NONSECURE_ENTRY
void pin_function_s(int32_t port_index, int32_t pin_index, int32_t data);

#ifdef __cplusplus
}
#endif

#endif